static FISH_INCUBATION_PERIOD: usize = 6;
static _NEW_FISH_INCUBATION_PERIOD: usize = 8;
static SIMULATION_DAYS: usize = 256;

fn main() {
    let mut fish: Vec<usize> = vec![0usize; 9];

    include_str!("../input.txt")
        .split(",")
        .for_each(|f| {
            fish[f.trim().parse::<usize>().unwrap()] += 1;
        });

    for _ in 0..SIMULATION_DAYS {
        fish.rotate_left(1);
        fish[FISH_INCUBATION_PERIOD] += *fish.last().unwrap();
    }

    println!(
        "Fish: {:?}",
        fish.iter().sum::<usize>()
    );
}
