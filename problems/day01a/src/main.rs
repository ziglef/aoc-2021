use itertools::Itertools;

fn main() {
    println!(
        "{}",
        include_str!("../input.txt")
            .lines()
            .map(|i| i.parse::<i32>().unwrap())
            .tuple_windows::<(i32, i32)>()
            .filter(|i| i.1 - i.0 > 0)
            .count()
    );
}
