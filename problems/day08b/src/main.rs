fn to_bits(s: &str) -> u8 {
    s.as_bytes()
        .iter()
        .fold(0_u8, |acc, b| acc + (1 << (b - b'a') as usize))
}

fn main() {
    let mut sum: usize = 0;
    let mut one= 0u8;
    let mut four= 0u8;

    include_str!("../input.txt")
        .lines()
        .for_each(|l| {
            let io: Vec<&str> = l.split(" | ").collect();

            for input in io[0].split_whitespace() {
                match input.len() {
                    2 => {
                        one = to_bits(input);
                    },
                    4 => {

                        four = to_bits(input);
                    },
                    _ => {}
                }
            }

            for (i, output) in io[1].split_whitespace().enumerate() {
                sum += match output.len() {
                    2 => 1 * 10usize.pow(3-i as u32),
                    3 => 7 * 10usize.pow(3-i as u32),
                    4 => 4 * 10usize.pow(3-i as u32),
                    7 => 8 * 10usize.pow(3-i as u32),
                    5 => {
                        if (one & to_bits(output)).count_ones() == 2 {
                            3 * 10usize.pow(3-i as u32)
                        } else if (four & to_bits(output)).count_ones() == 2 {
                            2 * 10usize.pow(3-i as u32)
                        } else {
                            5 * 10usize.pow(3-i as u32)
                        }
                    }
                    6 => {
                        if (four & to_bits(output)).count_ones() == 4 {
                            9 * 10usize.pow(3-i as u32)
                        } else if (one & to_bits(output)).count_ones() == 2 {
                            0 * 10usize.pow(3-i as u32)
                        } else {
                            6 * 10usize.pow(3-i as u32)
                        }
                    }
                    _ => 0
                };
            }
        });

    println!("{:?}", sum);
}
