fn abs_diff(a: usize, b: usize) -> usize {
    if a < b {
        b - a
    } else {
        a - b
    }
}

fn main() {
    let mut crabs: Vec<usize> = Vec::new();
    let mut crabs_costs: Vec<Vec<usize>> = Vec::new();
    let mut crabs_mins: Vec<usize> = Vec::new();
    let max_crab_dist;

    include_str!("../input.txt")
        .split(",")
        .for_each(|c| {
            crabs.push(c.trim_end().parse::<usize>().unwrap());
        });

    max_crab_dist = *crabs.iter().max().unwrap();
    for _ in 0usize..max_crab_dist {
        crabs_costs.push(Vec::new());
    }
    for &c in &crabs {
        for i in 0usize..max_crab_dist {
            crabs_costs[i].push((abs_diff(c, i))*(abs_diff(c, i)+1)/2);
        }
    }

    for cost_vec in crabs_costs {
        crabs_mins.push(cost_vec.iter().sum());
    }
    println!("{}", crabs_mins.iter().min().unwrap());
}
