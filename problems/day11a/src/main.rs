fn main() {
    let mut dumbos: Vec<usize> = Vec::with_capacity(100);
    let mut flashes: usize = 0;

    include_str!("../input.txt")
        .lines()
        .for_each(|l| {
            l.chars().for_each(|c| {
                dumbos.push(c.to_digit(10).unwrap() as usize);
            });
        });

    for _ in 0..100 {
        dumbos.iter_mut().for_each(|d| *d += 1);
        let mut go = 1;

        while go != 0 {
            for i in 0..dumbos.len() {
                if dumbos[i] > 9 {
                    dumbos[i] = 0;

                    // LEFT
                    if i % 10 != 0 {
                        if dumbos[i - 1] != 0 {
                            dumbos[i - 1] += 1;
                        }
                    }
                    // UP
                    if i >= 10 {
                        if dumbos[i - 10] != 0 {
                            dumbos[i - 10] += 1;
                        }
                    }
                    // RIGHT
                    if i % 10 != 9 {
                        if dumbos[i + 1] != 0 {
                            dumbos[i + 1] += 1;
                        }
                    }
                    // DOWN
                    if i < 90 {
                        if dumbos[i + 10] != 0 {
                            dumbos[i + 10] += 1;
                        }
                    }
                    // UP LEFT
                    if i >= 10 && i % 10 != 0 {
                        if dumbos[i - 11] != 0 {
                            dumbos[i - 11] += 1;
                        }
                    }
                    // UP RIGHT
                    if i >= 10 && i % 10 != 9 {
                        if dumbos[i - 9] != 0 {
                            dumbos[i - 9] += 1;
                        }
                    }
                    // DOWN LEFT
                    if i < 90 && i % 10 != 0 {
                        if dumbos[i + 9] != 0 {
                            dumbos[i + 9] += 1;
                        }
                    }
                    // DOWN RIGHT
                    if i < 90 && i % 10 != 9 {
                        if dumbos[i + 11] != 0 {
                            dumbos[i + 11] += 1;
                        }
                    }

                    flashes += 1;
                }
            }
            go = dumbos.iter().filter(|d| **d > 9).count();
        }
    }

    println!("{:?}", flashes);
}
