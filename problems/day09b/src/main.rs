static INPUT_WIDTH: usize = 100;
static INPUT_HEIGHT:usize  = 100;

fn get_basin_size(y: usize, x: usize, heightmap: &Vec<Vec<usize>>) -> usize {

    let mut size: usize = 1;
    let mut coords_explored: Vec<(usize, usize)> = Vec::new();
    let mut coords_to_explore: Vec<(usize, usize)> = Vec::new();

    coords_to_explore.push((y,x));

    while coords_to_explore.len() > 0 {
        let curr = coords_to_explore.pop().unwrap();
        if coords_explored.contains(&curr) {
            size -= 1;
            continue;
        } else {
            coords_explored.push(curr);
        }
        if curr.0 != 0 {
            if heightmap[curr.0-1][curr.1] > heightmap[curr.0][curr.1] && heightmap[curr.0-1][curr.1] != 9 {
                size += 1;
                coords_to_explore.push((curr.0-1, curr.1));
            }
        }
        if curr.0 != INPUT_HEIGHT - 1 {
            if heightmap[curr.0+1][curr.1] > heightmap[curr.0][curr.1] && heightmap[curr.0+1][curr.1] != 9 {
                size += 1;
                coords_to_explore.push((curr.0+1, curr.1));
            }
        }
        if curr.1 != 0 {
            if heightmap[curr.0][curr.1-1] > heightmap[curr.0][curr.1] && heightmap[curr.0][curr.1-1] != 9 {
                size += 1;
                coords_to_explore.push((curr.0, curr.1-1));
            }
        }
        if curr.1 != INPUT_WIDTH - 1 {
            if heightmap[curr.0][curr.1+1] > heightmap[curr.0][curr.1] && heightmap[curr.0][curr.1+1] != 9 {
                size += 1;
                coords_to_explore.push((curr.0, curr.1+1));
            }
        }
    }

    size
}

fn main() {
    let mut heightmap: Vec<Vec<usize>> = Vec::with_capacity(INPUT_HEIGHT);
    let mut basin_sizes: Vec<usize> = Vec::new();

    include_str!("../input.txt")
        .lines()
        .for_each(|l| {
            heightmap.push(Vec::with_capacity(INPUT_WIDTH));
            l.chars().for_each(|c|{
                heightmap.last_mut().unwrap().push(c.to_digit(10).unwrap() as usize);
            });
        });

    for (y, v) in heightmap.iter().enumerate() {
        for (x, vv) in v.iter().enumerate() {
            if *vv == 9 {
                continue;
            }
            if y != 0 {
                if heightmap[y-1][x] <= *vv {
                    continue;
                }
            }
            if y != INPUT_HEIGHT - 1 {
                if heightmap[y+1][x] <= *vv {
                    continue;
                }
            }
            if x != 0 {
                if heightmap[y][x-1] <= *vv {
                    continue;
                }
            }
            if x != INPUT_WIDTH - 1 {
                if heightmap[y][x+1] <= *vv {
                    continue;
                }
            }
            basin_sizes.push(get_basin_size(y, x, &heightmap));
        }
    }

    basin_sizes.sort();
    basin_sizes.reverse();
    basin_sizes.truncate(3);
    println!(
        "{:?}",
        basin_sizes[0] * basin_sizes[1] * basin_sizes[2]
    );
}
