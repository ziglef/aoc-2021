fn main() {
    let mut sums: [usize; 12] = [0; 12];
    let mut gamma: usize = 0;
    let mut epsilon: usize = 0;
    let mut input: usize = 0;

    include_str!("../input.txt")
        .lines()
        .map(|i| usize::from_str_radix(i, 2).unwrap())
        .for_each(|i| {
            for ib in 0..12 {
                sums[ib] += (i & (1 << ib)) >> ib
            }
            input += 1;
        });

    sums.iter().enumerate().for_each(|(i, &sum)| {
        if sum > input / 2 {
            gamma += 2usize.pow(i as u32);
        } else {
            epsilon += 2usize.pow(i as u32);
        }
    });

    println!("{}", gamma * epsilon);
}
