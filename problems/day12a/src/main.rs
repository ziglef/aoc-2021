use std::collections::HashMap;


fn visit_all_paths(connections: &HashMap<&str, Vec<&str>>, curr: &str, visited: &mut String) -> usize {
    let mut sum: usize = 0;
    for &connection in connections.get(curr).unwrap() {
        if connection == connection.to_lowercase() && visited.contains(connection) {
            continue;
        } else {
            visited.push_str(curr);
            if !curr.eq("end") {
                visited.push_str(",");
            } else {
                while visited.pop().unwrap() != ',' {}
                return 1;
            }
            sum += visit_all_paths(connections, connection, visited);
        }
    }
    while visited.pop().unwrap_or(',') != ',' {}

    sum
}


fn main() {
    let mut connections: HashMap<&str, Vec<&str>> = HashMap::new();

    include_str!("../input.txt")
        .lines()
        .for_each(|l| {
            let mut entry = l.split('-');
            let (start, end) = (entry.next().unwrap(), entry.next().unwrap());
            if connections.contains_key(start) {
                connections.get_mut(start).unwrap().push(end);
            } else {
                connections.insert(start, Vec::new());
                connections.get_mut(start).unwrap().push(end);
            }
            if connections.contains_key(end) {
                connections.get_mut(end).unwrap().push(start);
            } else {
                connections.insert(end, Vec::new());
                connections.get_mut(end).unwrap().push(start);
            }
        });

    println!(
        "{:?}",
        visit_all_paths(&connections, "start", &mut "".to_string())
    );
}
