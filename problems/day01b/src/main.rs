use itertools::Itertools;

fn main() {
    println!(
        "{}",
        include_str!("../input.txt")
            .lines()
            .map(|i| i.parse::<i32>().unwrap())
            .tuple_windows::<(i32, i32, i32)>()
            .tuple_windows::<((i32, i32, i32), (i32, i32, i32))>()
            .filter(|i| (i.1.0 + i.1.1 + i.1.2) - (i.0.0 + i.0.1 + i.0.2) > 0)
            .count()
    );
}
