use std::collections::HashMap;
use itertools::Itertools;

fn main() {
    let mut lines = include_str!("../input.txt").lines();
    let mut rules: HashMap<Vec<char>, char> = HashMap::new();
    let mut inserts: HashMap<usize, char> = HashMap::new();

    let mut polymer: Vec<char> = lines.next().unwrap().chars().collect::<Vec<char>>();
    lines.next();

    lines.for_each(|l| {
        let mut split = l.split(" -> ");
        rules.insert(split.next().unwrap().chars().collect::<Vec<char>>(), split.next().unwrap().chars().next().unwrap());
    });

    for _ in 0..10 {
        polymer.windows(2).enumerate().for_each(|t| {
            inserts.insert(t.0, *rules.get(t.1).unwrap());
        });

        for i in 0..inserts.len() {
            polymer.insert(1 + i + i, *inserts.get(&i).unwrap());
        }
        inserts.clear();
    }

    polymer.sort();
    let frequencies = polymer.iter().dedup_with_count();
    let max = frequencies.clone().max().unwrap();
    let min = frequencies.clone().min().unwrap();
    println!("Diff: {:?}", max.0 - min.0);
}
