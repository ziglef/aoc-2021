fn mark_hv_line(vents_map: &mut Vec<Vec<usize>>, start: &Vec<usize>, end: &Vec<usize>) {
    let mut xrange= start[0]..=end[0];
    if start[0] > end[0] {
        xrange = end[0]..=start[0];
    }

    let mut yrange = start[1]..=end[1];
    if start[1] > end[1] {
        yrange = end[1]..=start[1];
    }

    for x in xrange.clone() {
        for y in yrange.clone() {
            vents_map[y][x] += 1;
        }
    }
}

fn mark_d_line(vents_map: &mut Vec<Vec<usize>>, start: &Vec<usize>, end: &Vec<usize>) {

    let mut xs: Vec<usize> = Vec::new();
    if start[0] > end[0] {
        for x in end[0]..=start[0] {
            xs.push(x);
        }
        xs.reverse();
    } else {
        for x in start[0]..=end[0] {
            xs.push(x);
        }
    }

    let mut ys: Vec<usize> = Vec::new();
    if start[1] > end[1] {
        for y in end[1]..=start[1] {
            ys.push(y);
        }
        ys.reverse();
    } else {
        for y in start[1]..=end[1] {
            ys.push(y);
        }
    }

    let range= xs.iter().zip(ys.iter());

    for (x, y) in range {
        vents_map[*y][*x] += 1;
    }
}

fn main() {
    let mut inputs: Vec<Vec<Vec<usize>>> = Vec::new();

    include_str!("../input.txt")
        .lines()
        .for_each(|l| {
            inputs.push(
                l.split(" -> ")
                    .map(|i| {
                        i.split(",")
                            .map(|n| {
                                n.parse::<usize>().unwrap()
                            }).collect::<Vec<usize>>()
                    }).collect::<Vec<Vec<usize>>>()
            );
        });

    let map_size: usize = *inputs.iter().flatten().into_iter().flatten().max().unwrap() + 1;
    let mut vents_map: Vec<Vec<usize>> = vec![vec![0usize; map_size]; map_size];

    for vent in inputs {
        if vent[0][0] == vent[1][0] || vent[0][1] == vent[1][1] {
            mark_hv_line(&mut vents_map, &vent[0], &vent[1]);
        } else {
            mark_d_line(&mut vents_map, &vent[0], &vent[1]);
        }
    }

    println!(
        "{:?}",
        vents_map
            .iter()
            .flatten()
            .filter(|&&p| p > 1)
            .count()
    );
}
