use std::collections::HashMap;

fn main() {
    let mut lines = include_str!("../input.txt").lines();
    let mut rules: HashMap<Vec<char>, char> = HashMap::new();
    let mut pairs: HashMap<Vec<char>, usize> = HashMap::new();
    let mut sums: HashMap<char, usize> = HashMap::new();

    // INPUT
    let polymer: Vec<char> = lines.next().unwrap().chars().collect::<Vec<char>>();
    lines.next();

    lines.for_each(|l| {
        let mut split = l.split(" -> ");
        let pair = split.next().unwrap().chars().collect::<Vec<char>>();
        rules.insert(pair.clone(), split.next().unwrap().chars().next().unwrap());
        pairs.insert(pair, 0);
    });

    // PREPARATION
    for &c in &polymer {
        *sums.entry(c).or_insert(0usize) += 1;
    }
    polymer.windows(2).for_each(|t| {
        *pairs.get_mut(t).unwrap() += 1;
    });

    for _ in 0..40 {
        let keys = pairs.clone().keys().cloned().collect::<Vec<Vec<char>>>();
        let old_pairs = pairs.clone();
        keys.iter().for_each(|t| {

            if *old_pairs.get(t).unwrap() > 0 {
                *sums.entry(*rules.get(t).unwrap()).or_insert(0usize) += *old_pairs.get(t).unwrap();
                *pairs.get_mut(t).unwrap() -= *old_pairs.get(t).unwrap();
                *pairs.get_mut(&*Vec::from([*t.first().unwrap(), *rules.get(t).unwrap()])).unwrap() += *old_pairs.get(t).unwrap();
                *pairs.get_mut(&*Vec::from([*rules.get(t).unwrap(), *t.last().unwrap()])).unwrap() += *old_pairs.get(t).unwrap();
            }
        });
    }

    println!("Difference: {:?}", sums.values().max().unwrap() - sums.values().min().unwrap());
}
