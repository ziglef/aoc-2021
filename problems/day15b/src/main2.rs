use std::cmp::Ordering;
use std::collections::BinaryHeap;

#[derive(Copy, Clone, Eq, PartialEq)]
struct State {
    cost: usize,
    position: usize,
}

impl Ord for State {
    fn cmp(&self, other: &Self) -> Ordering {
        other.cost.cmp(&self.cost)
            .then_with(|| self.position.cmp(&other.position))
    }
}

impl PartialOrd for State {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

struct Edge {
    node: (usize, usize),
    cost: usize,
}

fn shortest_path(adj_list: &HashMap<(usize, usize), Vec<Edge>>, start: (usize, usize), goal: (usize, usize)) -> Option<usize> {
    let mut heap = BinaryHeap::new();
    let mut dist: HashMap<(usize, usize), usize> = HashMap::new();

    for key in adj_list.keys() {
        dist.insert(*key, usize::MAX);
    }
    *dist.get_mut(&start).unwrap() = 0usize;

    heap.push(State { cost: 0, position: start });

    while let Some(State { cost, position }) = heap.pop() {
        if position == goal { return Some(cost); }

        if cost > dist.get(position).unwrap() { continue; }

        for edge in &adj_list.get(position).unwrap() {
            let next = State { cost: cost + edge.cost, position: edge.node };

            if next.cost < dist.get(next.position).unwrap() {
                heap.push(next);
                dist.get_mut(next.position).unwrap() = next.cost;
            }
        }
    }

    None
}

fn main() {
    let mut risk_map: HashMap<(usize, usize), usize> = HashMap::new();
    include_str!("../input.txt")
        .lines()
        .enumerate().for_each(|(y, l)| {
        l.chars().enumerate().for_each(|(x, c)| {
            risk_map.insert((y, x), c.to_digit(10).unwrap() as usize);
        });
    });
    for y in 0usize..100 {
        for x in 0usize..100 {
            for step in 1usize..5 {
                let mut insert = risk_map.get(&(y, x)).unwrap() + step;
                if insert > 9 {
                    insert -= 9;
                }
                risk_map.insert((y, x+100*step), insert);
            }
        }
    }
    for y in 0usize..100 {
        for x in 0usize..500 {
            for step in 1usize..5 {
                let mut insert = risk_map.get(&(y, x)).unwrap() + step;
                if insert > 9 {
                    insert -= 9;
                }
                risk_map.insert((y+100*step, x), insert);
            }
        }
    }

    let graph: HashMap<(usize, usize), Vec<Edge>> = HashMap::new();
    for key in risk_map.keys() {
        let k_vec = Vec::new();
        if risk_map.contains_key(&(key.0 + 1, key.1)) {
            k_vec..push(Edge {node: (key.0 + 1, key.1), cost: risk_map.get((key.0 + 1, key.1)).unwrap()});
        }
        if risk_map.contains_key(&(key.0, key.1 + 1)) {
            k_vec..push(Edge {node: (key.0, key.1 + 1), cost: risk_map.get((key.0, key.1 + 1)).unwrap()});
        }
        if key.0 > 0 && risk_map.contains_key(&(key.0 - 1, key.1)) {
            k_vec..push(Edge {node: (key.0 - 1, key.1), cost: risk_map.get((key.0 - 1, key.1)).unwrap()});
        }
        if key.1 > 0 && risk_map.contains_key(&(key.0, key.1 - 1)) {
            k_vec..push(Edge {node: (key.0, key.1 - 1), cost: risk_map.get((key.0, key.1 - 1)).unwrap()});
        }
        graph.insert(key, k_vec);
    }

    println!("{:?}", shortest_path(&graph, (0, 0), (499,499)).unwrap());

    let graph = vec![
        // Node 0
        vec![Edge { node: 2, cost: 10 },
             Edge { node: 1, cost: 1 }],
        // Node 1
        vec![Edge { node: 3, cost: 2 }],
        // Node 2
        vec![Edge { node: 1, cost: 1 },
             Edge { node: 3, cost: 3 },
             Edge { node: 4, cost: 1 }],
        // Node 3
        vec![Edge { node: 0, cost: 7 },
             Edge { node: 4, cost: 2 }],
        // Node 4
        vec![]];
}