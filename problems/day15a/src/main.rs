use std::collections::HashMap;

fn map_min_key_in(map: &HashMap<(usize, usize), usize>, keys_map: &HashMap<(usize, usize), usize>) -> (usize, usize) {
    let mut min: usize = usize::MAX;
    let mut min_key: (usize, usize) = (usize::MAX, usize::MAX);

    for k in keys_map.keys() {
        if min > *map.get(k).unwrap() {
            min = *map.get(k).unwrap();
            min_key = *k;
        }
    }

    min_key
}

fn min_distance(risk_map: &HashMap<(usize, usize), usize>, source: (usize, usize), target: (usize, usize)) -> usize {
    let mut dist: HashMap<(usize, usize), usize> = HashMap::new();
    let mut prev: HashMap<(usize, usize), (usize, usize)> = HashMap::new();
    let mut cloned_map: HashMap<(usize, usize), usize> = risk_map.clone();

    for key in risk_map.keys() {
        dist.insert(*key, usize::MAX);
    }
    *dist.get_mut(&source).unwrap() = 0usize;

    while !cloned_map.is_empty() {
        let min_coords = map_min_key_in(&dist, &cloned_map);

        cloned_map.remove(&min_coords);

        if min_coords == target {
            let mut distance: usize = 0;
            let mut node = target;
            while prev.contains_key(&node) && node != source {
                distance += *risk_map.get(&node).unwrap();
                node = *prev.get(&node).unwrap();
            }
            return distance;
        }

        let mut dirs_to_check: Vec<(usize, usize)> = Vec::with_capacity(4);
        if risk_map.contains_key(&(min_coords.0 + 1, min_coords.1)) {
            dirs_to_check.push((min_coords.0 + 1, min_coords.1));
        }
        if risk_map.contains_key(&(min_coords.0, min_coords.1 + 1)) {
            dirs_to_check.push((min_coords.0, min_coords.1 + 1));
        }
        if min_coords.0 > 0 && risk_map.contains_key(&(min_coords.0 - 1, min_coords.1)) {
            dirs_to_check.push((min_coords.0 - 1, min_coords.1));
        }
        if min_coords.1 > 0 && risk_map.contains_key(&(min_coords.0, min_coords.1 - 1)) {
            dirs_to_check.push((min_coords.0, min_coords.1 - 1));
        }
        for dir in dirs_to_check {
            if risk_map.contains_key(&dir) {
                let alt = dist.get(&min_coords).unwrap() + risk_map.get(&dir).unwrap();
                if alt < *dist.get(&dir).unwrap() {
                    *dist.get_mut(&dir).unwrap() = alt;
                    prev.insert(dir, min_coords);
                }

            }
        }
    }
    0
}

fn main() {
    let mut risk_map: HashMap<(usize, usize), usize> = HashMap::new();
    include_str!("../input.txt")
        .lines()
        .enumerate().for_each(|(y, l)| {
        l.chars().enumerate().for_each(|(x, c)| {
            risk_map.insert((y, x), c.to_digit(10).unwrap() as usize);
        });
    });

    println!("{:?}", min_distance(&risk_map, (0, 0), (99,99)));
}
