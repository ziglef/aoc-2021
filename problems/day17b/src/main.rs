use std::ops::Range;

fn simulate_x(initial_velocity: isize, step: isize) -> isize {
    let mut x = 0;
    let mut velocity = initial_velocity;

    for _ in 0..step {
        x += velocity;
        if velocity > 0 {
            velocity -= 1;
        } else if velocity < 0 {
            velocity += 1;
        }
    }

    x
}

fn simulate_y(initial_velocity: isize, step: isize) -> isize {
    let mut y = 0;
    let mut velocity = initial_velocity;

    for _ in 0..step {
        y += velocity;
        velocity -= 1;
    }

    y
}

fn main() {
    // Ugly parsing because I am too lazy to write a regex
    let input = include_str!("../input.txt").lines().next().unwrap();
    let split_by_x = input.split("x=").last().unwrap();
    let mut x_parts = split_by_x.split("..");
    let split_by_y = split_by_x.split("y=").last().unwrap();
    let mut y_parts = split_by_y.split("..");
    let x_range: Range<isize> = x_parts.next().unwrap().parse::<isize>().unwrap()..x_parts.next().unwrap().split(",").next().unwrap().parse::<isize>().unwrap();
    let y_range: Range<isize> = y_parts.next().unwrap().parse::<isize>().unwrap()..y_parts.next().unwrap().split(",").next().unwrap().parse::<isize>().unwrap();

    println!("{:?} {:?} {:?}", x_range, x_range.start, x_range.end);
    println!("{:?} {:?} {:?}", y_range, y_range.start, y_range.end);

    let mut possible_results: Vec<(isize, isize)> = Vec::new();
    for x_vel in 0..=x_range.end {
        for y_vel in y_range.start..=y_range.start.abs() {
            let mut step = 0;
            loop {
                let (result_x, result_y) = (simulate_x(x_vel, step), simulate_y(y_vel, step));
                if result_x > x_range.end || result_y < y_range.start {
                    break;
                }
                if (x_range.start..=x_range.end).contains(&result_x) && (y_range.start..=y_range.end).contains(&result_y) {
                    possible_results.push((x_vel, y_vel));
                    break;
                }
                step += 1;
            }
        }
    }
    println!("{:?}", possible_results.iter().count());

}
