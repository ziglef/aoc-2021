use std::iter::Peekable;
use std::str::Lines;

fn mark_number(number: u32, card: &mut Vec<Vec<(u32, bool)>>) {
    for line in card {
        for (n, m) in line {
            if *n == number {
                *m = true
            }
        }
    }
}

fn check_for_bingo(number: u32, card: Vec<Vec<(u32, bool)>>) -> Option<u32> {
    let mut marked = card.clone();
    let mut unmarked = card.clone();

    let mut marked_iterators: Vec<_> = card.into_iter().map(|n| n.into_iter()).collect();
    let mut transposed_marked = (0..5)
        .map(|_| {
            marked_iterators
                .iter_mut()
                .map(|n| n.next().unwrap())
                .collect::<Vec<(u32, bool)>>()
        })
        .collect::<Vec<Vec<(u32, bool)>>>();

    for line in &mut marked {
        line.retain( |n| n.1 == true);
    }
    for line in &mut transposed_marked {
        line.retain( |n| n.1 == true);
    }
    for line in &mut unmarked {
        line.retain( |n| n.1 == false);
    }

    for line in &marked {
        if line.len() == 5 {
            return Some(unmarked.iter().flatten().fold(0,|acc, (n, _m)| acc + n) * number);
        }
    }

    for line in &transposed_marked {
        if line.len() == 5 {
            return Some(unmarked.iter().flatten().fold(0,|acc, (n, _m)| acc + n) * number);
        }
    }

    Some(0u32)
}

fn play_bingo_to_lose(numbers: Vec<u32>, cards: &mut Vec<Vec<Vec<(u32, bool)>>>) -> u32 {
    let mut no_cards = cards.len();
    let mut indexes_to_remove: Vec<usize> = vec![];
    for number in numbers {
        for (index, card) in cards.iter_mut().enumerate() {
            mark_number(number, card);
            let r = check_for_bingo(number, card.clone()).unwrap();
            if r != 0 {
                if no_cards > 1 {
                    indexes_to_remove.push(index);
                } else {
                    return r;
                }
            }
        }
        if indexes_to_remove.len() > 0 {
            indexes_to_remove.sort();
            indexes_to_remove.reverse();
            for index in &indexes_to_remove {
                cards.remove(*index);
            }
            no_cards = cards.len();
            indexes_to_remove.clear();
        }
    }
    0
}

fn extract_bingo_card(lines: &mut Peekable<Lines>) -> Vec<Vec<(u32, bool)>> {
    let mut card: Vec<Vec<(u32, bool)>> = vec![vec![(0, false); 5]; 5];

    for i in 0usize..5 {
        card[i] = lines
            .next()
            .unwrap()
            .split_whitespace()
            .map(|i| (i.parse::<u32>().unwrap(), false))
            .collect();
    }

    lines.next();
    card
}

fn main() {
    let mut input = include_str!("../input.txt").lines().peekable();
    let mut cards: Vec<Vec<Vec<(u32, bool)>>> = Vec::new();

    let numbers: Vec<u32> = input
        .next()
        .unwrap()
        .split(",")
        .map(|i| i.parse::<u32>().unwrap())
        .collect();
    input.next();

    while input.peek().is_some() {
        cards.push(extract_bingo_card(&mut input));
    }

    println!("{:?}", play_bingo_to_lose(numbers, &mut cards));
}
