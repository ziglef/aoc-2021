fn main() {
    let mut heightmap: Vec<Vec<usize>> = Vec::with_capacity(100);
    let mut low_points: Vec<usize> = Vec::new();

    include_str!("../input.txt")
        .lines()
        .for_each(|l| {
            heightmap.push(Vec::with_capacity(100));
            l.chars().for_each(|c|{
                heightmap.last_mut().unwrap().push(c.to_digit(10).unwrap() as usize);
            });
        });

    for (y, v) in heightmap.iter().enumerate() {
        for (x, vv) in v.iter().enumerate() {
            if *vv == 9 {
                continue;
            }
            if y != 0 {
                if heightmap[y-1][x] <= *vv {
                    continue;
                }
            }
            if y != 99 {
                if heightmap[y+1][x] <= *vv {
                    continue;
                }
            }
            if x != 0 {
                if heightmap[y][x-1] <= *vv {
                    continue;
                }
            }
            if x != 99 {
                if heightmap[y][x+1] <= *vv {
                    continue;
                }
            }
            low_points.push(*vv);
        }
    }

    println!(
        "{:?}",
        low_points.iter().fold(0, |mut sum, &lp| {sum += lp+1; sum})
    );
}
