fn main() {
    let mut unique_count = 0;

    include_str!("../input.txt")
        .lines()
        .for_each(|l| {
            unique_count += l.split(" | ").last().unwrap()
                .split_whitespace()
                .filter(|d|
                    d.len() == 7 || (d.len() >= 2 && d.len() <= 4)
                ).count();
        });

    println!("{:?}", unique_count);
}
