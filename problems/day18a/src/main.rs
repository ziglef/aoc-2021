fn magnitude(n: Vec<(usize, usize, char, usize)>) -> usize {
    let mut max_depth = 0;
    let mut number = n.clone();

    for pair in &number {
        if pair.1 > max_depth {
            max_depth = pair.1;
        }
    }

    for curr_depth in (1..=max_depth).rev() {
        let mut pair_indexes: Vec<usize> = Vec::new();

        for (index, pair) in number.iter().enumerate() {
            if pair.1 == curr_depth {
                pair_indexes.push(index);
            }
        }

        for i in (0..pair_indexes.len()).step_by(2) {
            let mut side = 'l';
            if pair_indexes[i] > 0 {
                if number[pair_indexes[i]-1].2 == 'l' && number[pair_indexes[i]-1].1 == number[pair_indexes[i]].1 - 1 {
                    side = 'r';
                }
            }
            number.insert(pair_indexes[i], (number[pair_indexes[i]].0, number[pair_indexes[i]].1-1, side, number[pair_indexes[i]].3*3 + number[pair_indexes[i+1]].3*2));
            number.remove(pair_indexes[i]+1);
            number.remove(pair_indexes[i]+1);
            for j in i+2..pair_indexes.len() {
                pair_indexes[j] -= 1;
            }
        }
    }

    number[0].3
}

fn main() {
    // order depth side value
    let mut numbers: Vec<Vec<(usize, usize, char, usize)>> = Vec::new();

    include_str!("../input.txt")
        .lines()
        .for_each(|l| {
            let mut order = 0;
            numbers.push(Vec::new());
            let mut stack: Vec<char> = Vec::new();
            l.chars().for_each(|c| {
                if c == '[' {
                    if stack.len() > 0 && *stack.last().unwrap() == ',' {
                        stack.pop();
                    }
                    stack.push(c);
                } else if c == ']' {
                    stack.pop();
                } else if c !=',' {
                    let side = if *stack.last().unwrap() == '[' {
                        order += 1;
                        'l'
                    } else {
                        stack.pop();
                        if numbers.last().unwrap().last().unwrap().2 == 'r' {
                            order += 1;
                        }
                        'r'
                    };

                    numbers.last_mut().unwrap().push((order-1, stack.len(), side, c.to_digit(10).unwrap() as usize));
                } else {
                    stack.push(c);
                }
            });
        });

    // println!("{:?}", numbers[0]);

    let mut result: Vec<(usize, usize, char, usize)> = Vec::new();
    for pair in numbers.get(0).unwrap() {
        result.push(*pair);
    }

    for _ in 1..numbers.len().clone() {
        let next_number = numbers.get(1).unwrap();
        let starting_order = result.last().unwrap().0 + 1;
        for pair in next_number {
            result.push((starting_order+pair.0, pair.1, pair.2, pair.3));
        }
        for pair in &mut result {
            pair.1 += 1;
        }
        numbers.remove(1);
        loop {
            // println!("{:?}", result);
            let mut keep_going = false;
            let mut broke = false;
            for (index, pair) in result.iter().cloned().enumerate() {
                // Explode
                if pair.1 > 4 {
                    // println!("EXPLOSION! {:?}", pair);
                    broke = true;
                    // Left explosion
                    if pair.2 == 'l' {
                        for i in (0..index).rev() {
                            if result[i].0 < pair.0 {
                                result[i].3 += pair.3;
                                break;
                            }
                        }
                    }
                    // Right explosion
                    if pair.2 == 'r' {
                        for i in index + 1..result.len() {
                            if result[i].0 > pair.0 {
                                result[i].3 += pair.3;
                                break;
                            }
                        }
                        let mut side = 'l';
                        if index > 0 {
                            if result[index-1].2 == 'l' && (result[index-1].1 == pair.1 - 1 || result[index-1].1 == pair.1) {
                                side = 'r';
                            }
                        }
                        result.insert(index + 1, (pair.0, pair.1 - 1, side, 0));
                        for i in index + 1..result.len() {
                            if result[i].0 > 0 && side == 'r' {
                                result[i].0 -= 1;
                            }
                        }
                    }
                    result.remove(index);
                    keep_going = true;
                    break;
                }
            }
            if !broke {
                for (index, pair) in result.iter().cloned().enumerate() {
                    // Split
                    if pair.3 > 9 {
                        // println!("SPLIT! {:?}", pair);
                        let mut has_pair = false;
                        // Check if exploding number has a pair
                        // Left search
                        if pair.2 == 'r' {
                            if index > 0 {
                                if result[index-1].2 == 'l' && result[index-1].0 == pair.0 {
                                    has_pair = true;
                                }
                            }
                        // Right search
                        } else if pair.2 == 'l' {
                            if index < result.len() {
                                if result[index+1].2 == 'r' && result[index+1].0 == pair.0 {
                                    has_pair = true;
                                }
                            }
                        }

                        if has_pair {
                            for i in index+1..result.len() {
                                if result[i].0 >= pair.0 {
                                    result[i].0 += 1;
                                }
                            }
                        }
                        let order = if has_pair && pair.2 == 'r'{
                            pair.0 + 1
                        } else {
                            pair.0
                        };
                        result.remove(index);
                        result.insert(index, (order, pair.1 + 1, 'r', (pair.3 + 1) / 2));
                        result.insert(index, (order, pair.1 + 1, 'l', pair.3 / 2));

                        keep_going = true;
                        break;
                    }
                }
            }
            if !keep_going {
                break;
            }
        }
    }
    // println!("{:?}", result);
    println!("{:?}", magnitude(result));
}
