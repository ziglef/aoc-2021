use std::array::IntoIter;
use std::collections::HashMap;

fn main() {
    let starters: Vec<char> = vec!['(', '[', '{', '<'];
    let cpairs: HashMap<char, char> = HashMap::from_iter(IntoIter::new( [(')', '('), (']', '['), ('}', '{'), ('>', '<')]));
    let points: HashMap<char, usize> = HashMap::from_iter(IntoIter::new( [(')', 3), (']', 57), ('}', 1197), ('>', 25137)]));

    let mut cstack: Vec<char> = Vec::with_capacity(128);
    let mut corrupteds: Vec<char> = Vec::with_capacity(128);

    include_str!("../input.txt")
        .lines()
        .for_each(|l| {
            for c in l.chars() {
                if starters.contains(&c) {
                    cstack.push(c);
                } else if cstack.ends_with(&[*cpairs.get(&c).unwrap()]) {
                    cstack.pop();
                } else {
                    corrupteds.push(c);
                    break;
                }
            };
        });

    println!("{:?}", corrupteds.iter().fold(0, |mut acc, c| {acc += points.get(c).unwrap(); acc} ));
}
