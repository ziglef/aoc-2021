fn extract_knowledge(mut bits: i32, most_frequent: usize, less_frequent: usize, mut inputs: Vec<usize>) -> usize {
    bits -= 1;
    while inputs.len() != 1  && bits >= 0{
        let freq_of_one = inputs.iter().filter(
            |&x| {
                (x & (1 << bits)) >> bits == 1
            }
        ).count();

        if freq_of_one >= (inputs.len() + 1) / 2 {
            inputs = inputs.into_iter().filter(|&x| {
                (x & (1 << bits)) >> bits == most_frequent
            }).collect();
        } else {
            inputs = inputs.into_iter().filter(|&x| {
                (x & (1 << bits)) >> bits == less_frequent
            }).collect();
        }

        bits -= 1;
    }
    inputs[0]
}

fn main() {
    let inputs: Vec<usize> = include_str!("../input.txt")
        .lines()
        .map(|i| usize::from_str_radix(i, 2).unwrap())
        .collect();

    let co2_scrubber_ratings: usize = extract_knowledge(12, 1, 0, inputs.clone());
    let oxygen_generator_ratings: usize = extract_knowledge(12, 0, 1, inputs.clone());

    println!("{:?}", oxygen_generator_ratings * co2_scrubber_ratings);
}
