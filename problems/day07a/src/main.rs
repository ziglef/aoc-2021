fn main() {
    let mut crabs: Vec<usize> = Vec::new();
    let mut sum: usize = 0;
    let median: usize;

    include_str!("../input.txt")
        .split(",")
        .for_each(|c| {
            crabs.push(c.trim_end().parse::<usize>().unwrap());
        });

    crabs.sort();

    if crabs.len() % 2 == 0 {
        median = (crabs[(crabs.len()-1)/2] + crabs[crabs.len()/2]) / 2;
    } else {
        median = crabs[crabs.len()/2];
    }

    crabs.iter().for_each( |&c| {
        if c < median {
            sum += median - c;
        } else {
            sum += c - median;
        }
    });
    println!("{}", sum);
}
