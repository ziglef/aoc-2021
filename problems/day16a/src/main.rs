#![feature(cursor_remaining)]
use std::io::Cursor;
use bitstream_io::{BigEndian, BitRead, BitReader};

fn parse_packet(mut reader: &mut BitReader<&mut Cursor<&Vec<u8>>, BigEndian>, align: bool) -> (usize, usize) {
    // Returns
    let mut sum: usize = 0;
    let mut bits_read: usize = 0;

    // Packet Header
    let version: u8 = reader.read(3).unwrap();
    println!("version {:?}", version);
    sum += version as usize;
    bits_read += 3;
    let type_id: u8 = reader.read(3).unwrap();
    println!("type_id {:?}", type_id);
    bits_read += 3;

    // Literal type
    if type_id == 4 {
        let mut literal: usize = 0;
        let mut literal_pieces: Vec<usize> = Vec::new();
        let mut keep_going: u8 = 1;

        // Read the bits from the number
        while keep_going != 0 {
            keep_going = reader.read(1).unwrap();
            bits_read += 1;
            literal_pieces.push(reader.read::<u8>(4).unwrap() as usize);
            bits_read += 4;
        }

        // Assemble the number
        let no_elements = literal_pieces.len();
        for s in 0..no_elements {
            literal += literal_pieces[s] << (4 * (no_elements - 1) - s * 4);
        }
        println!("literal {:?}", literal);
        if align { reader.byte_align(); }
    } else {
        let length_type: u8 = reader.read(1).unwrap();
        println!("length_type {:?}", length_type);
        bits_read += 1;

        if length_type == 0 {
            let mut subpackets_len: u32 = reader.read(15).unwrap();
            println!("subpackets_len {:?}", subpackets_len);
            bits_read += 15;
            let mut subpackets_bits: usize = 0;

            if subpackets_len < 8 {
                subpackets_len = 0;
            } else {
                subpackets_len = subpackets_len - 8;
            }

            while subpackets_bits < subpackets_len as usize {
                let subpacket = parse_packet(&mut reader, false);
                sum += subpacket.0;
                bits_read += subpacket.1;
                subpackets_bits += subpacket.1;
            }
            if align { reader.byte_align(); }
        } else {
            let subpacket_no: u16 = reader.read(11).unwrap();
            println!("subpacket_no {:?}", subpacket_no);
            bits_read += 11;
            for _ in 0..subpacket_no {
                let subpacket = parse_packet(&mut reader, false);
                sum += subpacket.0;
                bits_read += subpacket.1;
            }
            if align { reader.byte_align(); }
        }
    }
    (sum, bits_read)
}

fn main() {
    let input: Vec<u8> = hex::decode(include_str!("../input.txt").lines().next().unwrap()).expect("Decoding failed!");
    let mut cursor = Cursor::new(&input);
    let mut reader = BitReader::endian(&mut cursor, BigEndian);

    println!("{:?}", parse_packet(&mut reader, true));
}
