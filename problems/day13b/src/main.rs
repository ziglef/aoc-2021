
static MAX_X: usize = 1311;
static MAX_Y: usize = 1241;

fn abs_diff(a: usize, b: usize) -> usize {
    if a < b {
        b - a
    } else {
        a - b
    }
}

fn main() {
    let mut paper: Vec<Vec<bool>> = vec![vec![false; MAX_X]; MAX_Y];
    let mut folds: Vec<(char, usize)> = Vec::new();

    include_str!("../input.txt")
        .lines()
        .for_each(|l| {
            let mut split = l.split(',');

            let first = split.next().unwrap();
            if first.len() > 9 {
                let mut fold_split = first.split('=');
                folds.push((fold_split.next().unwrap().chars().last().unwrap(), fold_split.next().unwrap().parse::<usize>().unwrap()));
            } else if first.len() > 0 {
                let (x, y) = (
                    first.parse::<usize>().unwrap(),
                    split.next().unwrap().parse::<usize>().unwrap()
                );

                paper[y][x] = true;
            }
        });

    println!("Dots: {:?}", paper.iter().flatten().filter(|&&d| d == true).count());
    for (dir, coord) in folds {
        if dir == 'y' {
            for y in coord+1..MAX_Y {
                for x in 0usize..MAX_X {
                    paper[abs_diff(y, coord*2)][x] = paper[abs_diff(y, coord*2)][x] || paper[y][x];
                    paper[y][x] = false;
                }
            }
        } else {
            for y in 0..MAX_Y {
                for x in coord+1..MAX_X {
                    paper[y][abs_diff(x, coord*2)] = paper[y][abs_diff(x, coord*2)] || paper[y][x];
                    paper[y][x] = false;
                }
            }
        }
        println!("Dots: {:?}", paper.iter().flatten().filter(|&&d| d == true).count());
    }
    for y in 0usize..MAX_Y {
        for x in 0usize..MAX_X {
            if paper[y][x] == false {
                print!(".");
            } else {
                print!("#");
            }
        }
        println!();
    }
}
