use std::array::IntoIter;
use std::collections::HashMap;

fn main() {
    let starters: Vec<char> = vec!['(', '[', '{', '<'];
    let cpairs: HashMap<char, char> = HashMap::from_iter(IntoIter::new( [(')', '('), (']', '['), ('}', '{'), ('>', '<')]));
    let revcpairs: HashMap<char, char> = HashMap::from_iter(IntoIter::new( [('(', ')'), ('[', ']'), ('{', '}'), ('<', '>')]));
    let points: HashMap<char, usize> = HashMap::from_iter(IntoIter::new( [(')', 1), (']', 2), ('}', 3), ('>', 4)]));

    let mut cstack: Vec<char> = Vec::with_capacity(128);
    let mut scores: Vec<usize> = Vec::new();

    include_str!("../input.txt")
        .lines()
        .for_each(|l| {
            let mut score: usize = 0;
            for c in l.chars() {
                if starters.contains(&c) {
                    cstack.push(c);
                } else if cstack.ends_with(&[*cpairs.get(&c).unwrap()]) {
                    cstack.pop();
                } else {
                    cstack.clear();
                    break;
                }
            };
            cstack.reverse();
            for c in &cstack {
                score *= 5;
                score += points.get(revcpairs.get(c).unwrap()).unwrap();
            }
            cstack.clear();
            if score != 0 {
                scores.push(score);
            }
        });

    scores.sort();
    println!("{:?}", scores.get(scores.len()/2).unwrap());
}
